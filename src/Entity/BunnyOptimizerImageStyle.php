<?php

namespace Drupal\bunny_optimizer\Entity;

use Drupal\bunny_optimizer\Plugin\ImageToolkit\BunnyOptimizerToolkit;
use Drupal\bunny_optimizer\Plugin\ImageToolkit\BunnyOptimizerToolkitInterface;
use Drupal\image\Entity\ImageStyle;

/**
 * Overrides the image style configuration entity.
 *
 * This class customizes the url generation process to work with
 * Bunny Optimizer.
 */
class BunnyOptimizerImageStyle extends ImageStyle {

  /**
   * {@inheritdoc}
   */
  public function buildUri($uri) {
    /** @var \Drupal\Core\Image\ImageInterface $image */
    $image = \Drupal::service('image.factory')->get($uri);
    $toolkit = $image->getToolkit();

    if (!$toolkit instanceof BunnyOptimizerToolkitInterface) {
      return parent::buildUri($uri);
    }

    // We don't need to generate derivatives using this image toolkit,
    // so return the original url.
    return $uri;
  }

  /**
   * {@inheritdoc}
   */
  public function buildUrl($uri, $clean_urls = NULL) {
    /** @var \Drupal\Core\Image\ImageInterface $image */
    $image = \Drupal::service('image.factory')->get($uri);
    $toolkit = $image->getToolkit();

    if (!$toolkit instanceof BunnyOptimizerToolkitInterface) {
      return parent::buildUrl($uri, $clean_urls);
    }

    foreach ($this->getEffects() as $effect) {
      $effect->applyEffect($image);
    }

    $url = \Drupal::service('file_url_generator')->generateAbsoluteString($uri);
    $urlParts = parse_url($url);

    if (isset($urlParts['query'])) {
      parse_str($urlParts['query'], $queryParams);
      $queryParams += $toolkit->getParameters();
    }
    else {
      $queryParams = $toolkit->getParameters();
    }

    if ($cdnHostname = $toolkit->getCdnHostname()) {
      $urlParts['host'] = $cdnHostname;
    }

    if (!empty($urlParts['scheme'])) {
      $scheme = $urlParts['scheme'] . '://';
    } else {
      $scheme = '//';
    }

    return $scheme . $urlParts['host'] . $urlParts['path'] . '?' . http_build_query($queryParams);
  }

  /**
   * {@inheritdoc}
   */
  public function flush($path = NULL) {
    // Derivatives are the original image + some query parameters.
    // Don't call parent::flush() because we don't want to delete the original.
  }

  /**
   * {@inheritdoc}
   */
  public function supportsUri($uri) {
    // Only support the URI if its extension is supported by the current image
    // toolkit.
    return in_array(
      mb_strtolower(pathinfo($uri, PATHINFO_EXTENSION)),
      BunnyOptimizerToolkit::getSupportedInputExtensions(),
    );
  }

}
