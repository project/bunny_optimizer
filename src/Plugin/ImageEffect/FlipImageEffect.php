<?php

namespace Drupal\bunny_optimizer\Plugin\ImageEffect;

use Drupal\Core\Image\ImageInterface;
use Drupal\image\ImageEffectBase;

/**
 * An image effect to flip the output image vertically.
 *
 * @ImageEffect(
 *   id = "bunny_optimizer_flip",
 *   label = @Translation("Flip vertically"),
 * )
 */
class FlipImageEffect extends ImageEffectBase {

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image): bool {
    return $image->apply('bunny_optimizer_param', [
      'key' => 'flip',
      'value' => TRUE,
    ]);
  }

}
