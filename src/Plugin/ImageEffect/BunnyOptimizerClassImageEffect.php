<?php

namespace Drupal\bunny_optimizer\Plugin\ImageEffect;

use Drupal\bunny_optimizer\Entity\BunnyOptimizerImageStyle;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\image\ConfigurableImageEffectBase;
use Drupal\image\ImageEffectInterface;

/**
 * An image effect to control the output quality of lossy file formats.
 *
 * @ImageEffect(
 *   id = "bunny_optimizer_class",
 *   label = @Translation("Apply a Bunny Optimizer image class"),
 *   description = @Translation("Replaces a set of query parameters with a preset, configured in Bunny Optimizer."),
 * )
 */
class BunnyOptimizerClassImageEffect extends ConfigurableImageEffectBase {

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image): bool {
    return $image->apply('bunny_optimizer_param', [
      'key' => 'class',
      'value' => $this->configuration['class'],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $imageStyle = $form_state->getBuildInfo()['args'][0];
    assert($imageStyle instanceof BunnyOptimizerImageStyle);

    $messages = [];
    $messages['info'][] = $this->t('Image Classes replace a set of query parameters with a single, simple query parameter and offer significant benefits in multiple areas. You can find more information about this feature in the <a href="@announcementUrl">announcement blog post</a>.', [
      '@announcementUrl' => 'https://bunny.net/blog/introducing-bunny-optimizer-image-classes',
    ]);

    $otherEffects = array_filter(
      iterator_to_array($imageStyle->getEffects()),
      function (ImageEffectInterface $effect) {
        return $effect->uuid !== $this->uuid;
      }
    );
    if ($otherEffects !== []) {
      $messages['warning'][] = $this->t('This image style already has effects applied to it. Image classes are presets of parameters and must be used alone.');
    }

    $form['about'] = [
      '#theme' => 'status_messages',
      '#message_list' => $messages,
      '#status_headings' => [
        'status' => t('Status message'),
        'error' => t('Error message'),
        'warning' => t('Warning message'),
        'info' => t('Information message'),
      ],
    ];

    $form['class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Image class'),
      '#description' => $this->t('The name of the image class, as configured in Bunny Optimizer.'),
      '#default_value' => $this->configuration['class'] ?? NULL,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['class'] = $form_state->getValue('class');
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary(): array {
    $summary = parent::getSummary();
    $summary['#markup'] = $this->configuration['class'];

    return $summary;
  }

}
