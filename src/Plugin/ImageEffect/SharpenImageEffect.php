<?php

namespace Drupal\bunny_optimizer\Plugin\ImageEffect;

use Drupal\Core\Image\ImageInterface;
use Drupal\image\ImageEffectBase;

/**
 * An image effect to sharpen the output image.
 *
 * @ImageEffect(
 *   id = "bunny_optimizer_sharpen",
 *   label = @Translation("Sharpen"),
 * )
 */
class SharpenImageEffect extends ImageEffectBase {

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image): bool {
    return $image->apply('bunny_optimizer_param', [
      'key' => 'sharpen',
      'value' => TRUE,
    ]);
  }

}
