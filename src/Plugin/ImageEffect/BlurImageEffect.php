<?php

namespace Drupal\bunny_optimizer\Plugin\ImageEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\image\ConfigurableImageEffectBase;

/**
 * An image effect to blur the output image.
 *
 * @ImageEffect(
 *   id = "bunny_optimizer_blur",
 *   label = @Translation("Blur"),
 * )
 */
class BlurImageEffect extends ConfigurableImageEffectBase {

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image): bool {
    return $image->apply('bunny_optimizer_param', [
      'key' => 'blur',
      'value' => $this->configuration['blur'],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['blur'] = [
      '#type' => 'number',
      '#title' => $this->t('Blur'),
      '#description' => 'A number between 1 and 100.',
      '#default_value' => $this->configuration['quality'] ?? NULL,
      '#required' => TRUE,
      '#min' => 1,
      '#max' => 100,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['blur'] = (int) $form_state->getValue('blur');
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary(): array {
    $summary = parent::getSummary();
    $summary['#markup'] = $this->configuration['blur'];

    return $summary;
  }

}
