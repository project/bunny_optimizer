<?php

namespace Drupal\bunny_optimizer\Plugin\ImageEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\image\ConfigurableImageEffectBase;

/**
 * An image effect to adjust the brightness of the output image.
 *
 * @ImageEffect(
 *   id = "bunny_optimizer_auto_optimize",
 *   label = @Translation("Automatically optimize"),
 * )
 */
class AutoOptimizeImageEffect extends ConfigurableImageEffectBase {

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image): bool {
    return $image->apply('bunny_optimizer_param', [
      'key' => 'auto_optimize',
      'value' => $this->configuration['auto_optimize'],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['auto_optimize'] = [
      '#type' => 'radios',
      '#title' => $this->t('Automatic optimization'),
      '#description' => 'Automatically enhance the output image with multiple levels of optimizations. With high, sharpening is also applied automatically.',
      '#options' => $this->getOptions(),
      '#default_value' => $this->configuration['auto_optimize'] ?? NULL,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['auto_optimize'] = (int) $form_state->getValue('auto_optimize');
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary(): array {
    $summary = parent::getSummary();
    $summary['#markup'] = $this->getOptions()[$this->configuration['auto_optimize']] ?? NULL;

    return $summary;
  }

  /**
   * Get the possible options.
   *
   * @return array
   *   An array with the values as keys and the labels as values.
   */
  protected function getOptions(): array {
    return [
      'low' => $this->t('Low'),
      'medium' => $this->t('Medium'),
      'high' => $this->t('High'),
    ];
  }

}
