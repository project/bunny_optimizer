<?php

namespace Drupal\bunny_optimizer\Plugin\ImageEffect;

use Drupal\Core\Image\ImageInterface;
use Drupal\image\ImageEffectBase;

/**
 * An image effect to flip the output image horizontally.
 *
 * @ImageEffect(
 *   id = "bunny_optimizer_flop",
 *   label = @Translation("Flip horizontally"),
 * )
 */
class FlopImageEffect extends ImageEffectBase {

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image): bool {
    return $image->apply('bunny_optimizer_param', [
      'key' => 'flop',
      'value' => TRUE,
    ]);
  }

}
