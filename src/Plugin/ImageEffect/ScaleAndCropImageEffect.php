<?php

namespace Drupal\bunny_optimizer\Plugin\ImageEffect;

use Drupal\bunny_optimizer\Plugin\ImageToolkit\BunnyOptimizerToolkit;
use Drupal\Component\Utility\Image;
use Drupal\Core\Image\ImageInterface;
use Drupal\image\Plugin\ImageEffect\ScaleAndCropImageEffect as ScaleAndCropImageEffectBase;

/**
 * Extends the default 'Scale and crop' image effect.
 */
class ScaleAndCropImageEffect extends ScaleAndCropImageEffectBase {

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image): bool {
    if (!$image->getToolkit() instanceof BunnyOptimizerToolkit) {
      return parent::applyEffect($image);
    }

    /* @see https://stackoverflow.com/q/1373035 */
    $innerWidth = $this->configuration['width'];
    $innerHeight = $this->configuration['height'];
    $outerWidth = $image->getWidth() ?? 0;
    $outerHeight = $image->getHeight() ?? 0;

    $scale = min($outerWidth / $innerWidth, $outerHeight / $innerHeight);
    $resultWidth = $innerWidth * $scale;
    $resultHeight = $innerHeight * $scale;

    [$x, $y] = explode('-', $this->configuration['anchor']);
    if (function_exists('image_filter_keyword')) {
      $x = image_filter_keyword($x, $outerWidth, $resultWidth);
      $y = image_filter_keyword($y, $outerHeight, $resultHeight);
    }
    else {
      $x = Image::getKeywordOffset($x, $outerWidth, $resultWidth);
      $y = Image::getKeywordOffset($y, $outerHeight, $resultHeight);
    }

    $result = $image->apply('scale_and_crop', [
      'width' => $resultWidth,
      'height' => $resultHeight,
      'x' => $x,
      'y' => $y,
      'scaleWidth' => $innerWidth,
      'scaleHeight' => $innerHeight,
    ]);

    if (!$result) {
      $this->logger->error('Image scale and crop failed using the %toolkit toolkit on %path (%mimetype, %dimensions)', [
        '%toolkit' => $image->getToolkitId(),
        '%path' => $image->getSource(),
        '%mimetype' => $image->getMimeType() ?? 'unknown MIME type',
        '%dimensions' => ($image->getWidth() && $image->getHeight()) ? $image->getWidth() . 'x' . $image->getHeight() : 'unknown dimensions',
      ]);
      return FALSE;
    }

    return TRUE;
  }

}
