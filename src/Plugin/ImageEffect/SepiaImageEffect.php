<?php

namespace Drupal\bunny_optimizer\Plugin\ImageEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\image\ConfigurableImageEffectBase;

/**
 * An image effect to adjust the brightness of the output image.
 *
 * @ImageEffect(
 *   id = "bunny_optimizer_sepia",
 *   label = @Translation("Sepia"),
 * )
 */
class SepiaImageEffect extends ConfigurableImageEffectBase {

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image): bool {
    return $image->apply('bunny_optimizer_param', [
      'key' => 'sepia',
      'value' => $this->configuration['sepia'],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['sepia'] = [
      '#type' => 'radios',
      '#title' => $this->t('Sepia'),
      '#description' => 'Changes the image color to the sepia color scheme.',
      '#options' => $this->getOptions(),
      '#default_value' => $this->configuration['sepia'] ?? NULL,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['sepia'] = (int) $form_state->getValue('sepia');
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary(): array {
    $summary = parent::getSummary();
    $summary['#markup'] = $this->getOptions()[$this->configuration['sepia']] ?? NULL;

    return $summary;
  }

  /**
   * Get the possible options.
   *
   * @return array
   *   An array with the values as keys and the labels as values.
   */
  protected function getOptions(): array {
    return [
      'low' => $this->t('Low'),
      'medium' => $this->t('Medium'),
      'high' => $this->t('High'),
    ];
  }

}
