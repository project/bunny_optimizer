<?php

namespace Drupal\bunny_optimizer\Plugin\ImageEffect;

use Drupal\Core\Image\ImageInterface;
use Drupal\image\ImageEffectBase;

/**
 * An image effect to automatically frame crops around the main subject.
 *
 * @ImageEffect(
 *   id = "face_crop",
 *   label = @Translation("Smart Face Crop"),
 * )
 */
class FaceCropImageEffect extends ImageEffectBase {

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image): bool {
    return $image->apply('bunny_optimizer_param', [
      'key' => 'face_crop',
      'value' => 'true',
    ]);
  }

}
