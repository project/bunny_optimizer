<?php

namespace Drupal\bunny_optimizer\Plugin\ImageEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\image\ConfigurableImageEffectBase;

/**
 * An image effect to adjust the brightness of the output image.
 *
 * @ImageEffect(
 *   id = "bunny_optimizer_hue",
 *   label = @Translation("Hue"),
 *   description = @Translation("Adjust the hue of the output image by rotating the color wheel."),
 * )
 */
class HueImageEffect extends ConfigurableImageEffectBase {

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image): bool {
    return $image->apply('bunny_optimizer_param', [
      'key' => 'hue',
      'value' => $this->configuration['hue'],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['hue'] = [
      '#type' => 'number',
      '#title' => $this->t('Hue'),
      '#description' => 'A number between 0 and 100. The default value of 0 is the base color and increasing the value modulates to the next color for each 33 change.',
      '#default_value' => $this->configuration['hue'] ?? NULL,
      '#required' => TRUE,
      '#min' => 0,
      '#max' => 100,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['hue'] = (int) $form_state->getValue('hue');
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary(): array {
    $summary = parent::getSummary();
    $summary['#markup'] = $this->configuration['hue'];

    return $summary;
  }

}
