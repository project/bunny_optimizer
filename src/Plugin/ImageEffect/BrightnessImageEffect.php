<?php

namespace Drupal\bunny_optimizer\Plugin\ImageEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\image\ConfigurableImageEffectBase;

/**
 * An image effect to adjust the brightness of the output image.
 *
 * @ImageEffect(
 *   id = "bunny_optimizer_brightness",
 *   label = @Translation("Brightness"),
 * )
 */
class BrightnessImageEffect extends ConfigurableImageEffectBase {

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image): bool {
    return $image->apply('bunny_optimizer_param', [
      'key' => 'brightness',
      'value' => $this->configuration['brightness'],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['brightness'] = [
      '#type' => 'number',
      '#title' => $this->t('Brightness'),
      '#description' => 'A number between -100 and 100. A negative number darkens, a positive number brightens.',
      '#default_value' => $this->configuration['brightness'] ?? NULL,
      '#required' => TRUE,
      '#min' => -100,
      '#max' => 100,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['brightness'] = (int) $form_state->getValue('brightness');
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary(): array {
    $summary = parent::getSummary();
    $summary['#markup'] = $this->configuration['brightness'];

    return $summary;
  }

}
