<?php

namespace Drupal\bunny_optimizer\Plugin\ImageEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\image\ConfigurableImageEffectBase;

/**
 * An image effect to adjust the brightness of the output image.
 *
 * @ImageEffect(
 *   id = "bunny_optimizer_saturation",
 *   label = @Translation("Saturation"),
 * )
 */
class SaturationImageEffect extends ConfigurableImageEffectBase {

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image): bool {
    return $image->apply('bunny_optimizer_param', [
      'key' => 'saturation',
      'value' => $this->configuration['saturation'],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['saturation'] = [
      '#type' => 'number',
      '#title' => $this->t('Saturation'),
      '#description' => 'A number between -100 and 100.',
      '#default_value' => $this->configuration['saturation'] ?? NULL,
      '#required' => TRUE,
      '#min' => 0,
      '#max' => 100,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['saturation'] = (int) $form_state->getValue('saturation');
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary(): array {
    $summary = parent::getSummary();
    $summary['#markup'] = $this->configuration['saturation'];

    return $summary;
  }

}
