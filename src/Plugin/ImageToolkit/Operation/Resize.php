<?php

namespace Drupal\bunny_optimizer\Plugin\ImageToolkit\Operation;

use Drupal\Core\ImageToolkit\ImageToolkitOperationBase;

/**
 * Resizes an image to the given width and height (ignoring aspect ratio).
 *
 * @see https://docs.bunny.net/docs/stream-image-processing#width
 * @see https://docs.bunny.net/docs/stream-image-processing#height
 *
 * @ImageToolkitOperation(
 *   id = "bunny_optimizer_resize",
 *   toolkit = "bunny_optimizer",
 *   operation = "resize",
 *   label = @Translation("Resize"),
 *   description = @Translation("Resizes an image to the given width and height (ignoring aspect ratio).")
 * )
 *
 * @method \Drupal\bunny_optimizer\Plugin\ImageToolkit\BunnyOptimizerToolkit getToolkit()
 */
class Resize extends ImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'width' => [
        'description' => 'The new width of the resized image, in pixels',
      ],
      'height' => [
        'description' => 'The new height of the resized image, in pixels',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    // Assure integers for all arguments.
    $arguments['width'] = (int) round($arguments['width']);
    $arguments['height'] = (int) round($arguments['height']);

    // Fail when width or height are 0 or negative.
    if ($arguments['width'] <= 0) {
      throw new \InvalidArgumentException(sprintf('Invalid width (\'%s\') specified for the image \'resize\' operation', $arguments['width']));
    }
    if ($arguments['height'] <= 0) {
      throw new \InvalidArgumentException(sprintf('Invalid height (\'%d\') specified for the image \'resize\' operation', $arguments['height']));
    }

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(array $arguments = []): bool {
    $this->getToolkit()
      ->setParameter('width', $arguments['width'])
      ->setParameter('height', $arguments['height']);

    return TRUE;
  }

}
