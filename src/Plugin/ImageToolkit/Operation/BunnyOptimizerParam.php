<?php

namespace Drupal\bunny_optimizer\Plugin\ImageToolkit\Operation;

use Drupal\Core\ImageToolkit\ImageToolkitOperationBase;

/**
 * Applies a certain Bunny Optimizer parameter.
 *
 * @ImageToolkitOperation(
 *   id = "bunny_optimizer_param",
 *   toolkit = "bunny_optimizer",
 *   operation = "bunny_optimizer_param",
 *   label = @Translation("Apply Bunny Optimizer parameter"),
 *   description = @Translation("Applies a certain Bunny Optimizer parameter."),
 * )
 *
 * @method \Drupal\bunny_optimizer\Plugin\ImageToolkit\BunnyOptimizerToolkit getToolkit()
 */
class BunnyOptimizerParam extends ImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'key' => [
        'description' => 'Parameter',
      ],
      'value' => [
        'description' => 'Value',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(array $arguments): bool {
    $this->getToolkit()->setParameter($arguments['key'], $arguments['value']);

    return TRUE;
  }

}
