<?php

namespace Drupal\bunny_optimizer\Plugin\ImageToolkit\Operation;

use Drupal\Core\ImageToolkit\ImageToolkitOperationBase;

/**
 * Crops an image to a rectangle specified by the given dimensions.
 *
 * @see https://docs.bunny.net/docs/stream-image-processing#crop
 *
 * @ImageToolkitOperation(
 *   id = "bunny_optimizer_crop",
 *   toolkit = "bunny_optimizer",
 *   operation = "crop",
 *   label = @Translation("Crop"),
 *   description = @Translation("Crops an image to a rectangle specified by the given dimensions.")
 * )
 *
 * @method \Drupal\bunny_optimizer\Plugin\ImageToolkit\BunnyOptimizerToolkit getToolkit()
 */
class Crop extends ImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'x' => [
        'description' => 'The starting x offset at which to start the crop, in pixels',
      ],
      'y' => [
        'description' => 'The starting y offset at which to start the crop, in pixels',
      ],
      'width' => [
        'description' => 'The width of the cropped area, in pixels',
        'required' => FALSE,
        'default' => NULL,
      ],
      'height' => [
        'description' => 'The height of the cropped area, in pixels',
        'required' => FALSE,
        'default' => NULL,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    // Assure at least one dimension.
    if (empty($arguments['width']) && empty($arguments['height'])) {
      throw new \InvalidArgumentException("At least one dimension ('width' or 'height') must be provided to the image 'crop' operation");
    }

    // Assure integers for all arguments.
    foreach (['x', 'y', 'width', 'height'] as $key) {
      $arguments[$key] = (int) round($arguments[$key]);
    }

    // Fail when width or height are 0 or negative.
    if ($arguments['width'] <= 0) {
      throw new \InvalidArgumentException(sprintf('Invalid width (\'%s\') specified for the image \'crop\' operation', $arguments['width']));
    }
    if ($arguments['height'] <= 0) {
      throw new \InvalidArgumentException(sprintf('Invalid height (\'%s\') specified for the image \'crop\' operation', $arguments['height']));
    }

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(array $arguments): bool {
    $this->getToolkit()->setParameter('crop', implode(',', [
      $arguments['width'],
      $arguments['height'],
      $arguments['x'],
      $arguments['y'],
    ]));

    return TRUE;
  }

}
