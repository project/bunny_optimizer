<?php

namespace Drupal\bunny_optimizer\Plugin\ImageToolkit\Operation;

use Drupal\Core\ImageToolkit\ImageToolkitOperationBase;

/**
 * Scales an image to the exact width and height given.
 *
 * This plugin achieves the target aspect ratio by cropping the original image
 * equally on both sides, or equally on the top and bottom. This function is
 * useful to create uniform sized avatars from larger images.
 *
 * @see https://docs.bunny.net/docs/stream-image-processing#width
 * @see https://docs.bunny.net/docs/stream-image-processing#height
 * @see https://docs.bunny.net/docs/stream-image-processing#crop-gravity
 *
 * @ImageToolkitOperation(
 *   id = "bunny_optimizer_scale_and_crop",
 *   toolkit = "bunny_optimizer",
 *   operation = "scale_and_crop",
 *   label = @Translation("Scale and crop"),
 *   description = @Translation("Scales an image to the exact width and height given. This plugin achieves the target aspect ratio by cropping the original image equally on both sides, or equally on the top and bottom. This function is useful to create uniform sized avatars from larger images.")
 * )
 *
 * @method \Drupal\bunny_optimizer\Plugin\ImageToolkit\BunnyOptimizerToolkit getToolkit()
 */
class ScaleAndCrop extends ImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'x' => [
        'description' => 'The horizontal offset for the start of the crop, in pixels',
        'required' => FALSE,
        'default' => NULL,
      ],
      'y' => [
        'description' => 'The vertical offset for the start the crop, in pixels',
        'required' => FALSE,
        'default' => NULL,
      ],
      'width' => [
        'description' => 'The width to crop the image to, in pixels',
        'required' => TRUE,
      ],
      'height' => [
        'description' => 'The height to crop the image to, in pixels',
        'required' => TRUE,
      ],
      'scaleWidth' => [
        'description' => 'The width to resize the image to after cropping, in pixels',
        'required' => TRUE,
      ],
      'scaleHeight' => [
        'description' => 'The height to resize the image to after cropping, in pixels',
        'required' => TRUE,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    if ($arguments['width'] <= 0) {
      throw new \InvalidArgumentException("Invalid width ('{$arguments['width']}') specified for the image 'scale_and_crop' operation");
    }

    if ($arguments['height'] <= 0) {
      throw new \InvalidArgumentException("Invalid height ('{$arguments['height']}') specified for the image 'scale_and_crop' operation");
    }

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(array $arguments = []): bool {
    $resize = [
      'width' => $arguments['scaleWidth'],
      'height' => $arguments['scaleHeight'],
    ];

    return $this->getToolkit()->apply('resize', $resize)
      && $this->getToolkit()->apply('crop', $arguments);
  }

}
