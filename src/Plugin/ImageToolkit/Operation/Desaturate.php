<?php

namespace Drupal\bunny_optimizer\Plugin\ImageToolkit\Operation;

use Drupal\Core\ImageToolkit\ImageToolkitOperationBase;

/**
 * Converts an image to grayscale.
 *
 * @see https://docs.bunny.net/docs/stream-image-processing#saturation
 *
 * @ImageToolkitOperation(
 *   id = "bunny_optimizer_desaturate",
 *   toolkit = "bunny_optimizer",
 *   operation = "desaturate",
 *   label = @Translation("Desaturate"),
 *   description = @Translation("Converts an image to grayscale.")
 * )
 *
 * @method \Drupal\bunny_optimizer\Plugin\ImageToolkit\BunnyOptimizerToolkit getToolkit()
 */
class Desaturate extends ImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    // This operation does not use any parameters.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(array $arguments) {
    $this->getToolkit()->setParameter('saturation', -100);
  }

}
