<?php

namespace Drupal\bunny_optimizer\Plugin\ImageToolkit\Operation;

use Drupal\Core\ImageToolkit\ImageToolkitOperationBase;

/**
 * Resizes an image to the given width or height (maintaining aspect ratio).
 *
 * @see https://docs.bunny.net/docs/stream-image-processing#width
 * @see https://docs.bunny.net/docs/stream-image-processing#height
 *
 * @ImageToolkitOperation(
 *   id = "bunny_optimizer_scale",
 *   toolkit = "bunny_optimizer",
 *   operation = "scale",
 *   label = @Translation("Scale"),
 *   description = @Translation("Resizes an image to the given width or height (maintaining aspect ratio).")
 * )
 *
 * @method \Drupal\bunny_optimizer\Plugin\ImageToolkit\BunnyOptimizerToolkit getToolkit()
 */
class Scale extends ImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'width' => [
        'description' => 'The target width, in pixels. This value is omitted then the scaling will based only on the height value',
        'required' => FALSE,
        'default' => NULL,
      ],
      'height' => [
        'description' => 'The target height, in pixels. This value is omitted then the scaling will based only on the width value',
        'required' => FALSE,
        'default' => NULL,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    // Assure at least one dimension.
    if (empty($arguments['width']) && empty($arguments['height'])) {
      throw new \InvalidArgumentException("At least one dimension ('width' or 'height') must be provided to the image 'scale' operation");
    }

    if (!empty($arguments['width'])) {
      $arguments['width'] = (int) round($arguments['width']);

      if ($arguments['width'] <= 0) {
        throw new \InvalidArgumentException(sprintf("Invalid width ('%s') specified for the image 'scale' operation", $arguments['width']));
      }
    }

    if (!empty($arguments['height'])) {
      $arguments['height'] = (int) round($arguments['height']);

      if ($arguments['height'] <= 0) {
        throw new \InvalidArgumentException(sprintf("Invalid height ('%s') specified for the image 'scale' operation", $arguments['height']));
      }
    }

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(array $arguments = []): bool {
    $toolkit = $this->getToolkit();

    if (isset($arguments['width'])) {
      $toolkit->setParameter('width', $arguments['width']);
    }

    if (isset($arguments['height'])) {
      $toolkit->setParameter('height', $arguments['height']);
    }

    return TRUE;
  }

}
