<?php

namespace Drupal\bunny_optimizer\Plugin\ImageToolkit;

use Drupal\Core\ImageToolkit\ImageToolkitInterface;

/**
 * An image toolkit for Bunny Optimizer, a real-time image processing service.
 */
interface BunnyOptimizerToolkitInterface extends ImageToolkitInterface {

  /**
   * Get a parameter by key.
   */
  public function getParameter(string $key);

  /**
   * Set a parameter by key.
   */
  public function setParameter(string $key, string $value);

  /**
   * Unset a parameter by key.
   */
  public function unsetParameter(string $key);

  /**
   * Merge an array of parameters with the currently stored parameters.
   */
  public function mergeParameters(array $params);

  /**
   * Get all stored parameters.
   */
  public function getParameters(): array;

  /**
   * Get the CDN hostname from which your images are served.
   */
  public function getCdnHostname(): ?string;

  /**
   * Lists the supported input formats.
   *
   * @return string[]
   *   An array of file extensions.
   *
   * @see https://docs.bunny.net/docs/formats#supported-input-formats
   */
  public static function getSupportedInputExtensions(): array;

  /**
   * Lists the supported output formats.
   *
   * @return string[]
   *   An array of file extensions.
   *
   * @see https://docs.bunny.net/docs/formats#supported-output-formats
   */
  public static function getSupportedOutputExtensions(): array;

}
