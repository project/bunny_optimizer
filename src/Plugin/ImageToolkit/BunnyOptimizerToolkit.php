<?php

namespace Drupal\bunny_optimizer\Plugin\ImageToolkit;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\ImageToolkit\ImageToolkitBase;
use Drupal\Core\Render\Element;
use Drupal\file_mdm\FileMetadataManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * An image toolkit for Bunny Optimizer, a real-time image processing service.
 *
 * @ImageToolkit(
 *   id = "bunny_optimizer",
 *   title = @Translation("Bunny Optimizer toolkit")
 * )
 */
class BunnyOptimizerToolkit extends ImageToolkitBase implements BunnyOptimizerToolkitInterface {

  /**
   * A static cache for the width of the image.
   *
   * @var int
   */
  protected $width;

  /**
   * A static cache for the height of the image.
   *
   * @var int
   */
  protected $height;

  /**
   * The mime type of the image.
   *
   * @var string
   */
  protected $mimeType;

  /**
   * The params that will be sent to Bunny Optimizer along with the image.
   *
   * @var array
   */
  protected $params = [];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('image.toolkit.operation.manager'),
      $container->get('logger.channel.image'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isValid(): bool {
    // Let's assume the image is valid, this toolkit doesn't care.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function save($destination) {
    // No changes to the actual files are necessary to use this toolkit.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function parseFile(): bool {
    // Let's assume the image is valid, this toolkit doesn't care.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getWidth(): ?int {
    if (isset($this->width)) {
      return $this->width;
    }

    $this->loadInfo();

    return $this->width;
  }

  /**
   * {@inheritdoc}
   */
  public function getHeight(): ?int {
    if (isset($this->height)) {
      return $this->height;
    }

    $this->loadInfo();

    return $this->height;
  }

  /**
   * {@inheritdoc}
   */
  public function getMimeType(): ?string {
    if (isset($this->mimeType)) {
      return $this->mimeType;
    }

    $this->loadInfo();

    return $this->mimeType;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('bunny_optimizer.settings');

    $messages = [];
    $messages['info'][] = $this->t('Bunny Optimizer allows you to resize, crop, and modify images on the fly with simple url query parameters. You can find more information on the <a href="@websiteUrl">website</a>.', [
      '@websiteUrl' => 'https://bunny.net/optimizer',
    ]);

    $form['about'] = [
      '#theme' => 'status_messages',
      '#message_list' => $messages,
      '#status_headings' => [
        'status' => t('Status message'),
        'error' => t('Error message'),
        'warning' => t('Warning message'),
        'info' => t('Information message'),
      ],
    ];

    $form['cdn_hostname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CDN hostname'),
      '#description' => $this->t('The hostname as configured in Bunny CDN. In case the hostname of this website is linked to Bunny CDN with the intention of caching full page responses, this field can be left empty.'),
      '#default_value' => $config->get('cdn_hostname'),
    ];

    foreach (Element::children($form) as $child) {
      if ($config->get($child) !== $config->getOriginal($child, FALSE)) {
        $form[$child]['#disabled'] = TRUE;
        $form[$child]['#description'] = $this->t('This config cannot be changed because it is overridden.');
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('bunny_optimizer.settings');
    $values = $form_state->getValue('bunny_optimizer');

    foreach ($values as $key => $value) {
      if ($config->get($key) !== $config->getOriginal($key, FALSE)) {
        // Value has override.
        continue;
      }

      $config->set($key, $value);
    }

    $config->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getParameter(string $key) {
    return $this->params[$key] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setParameter(string $key, string $value) {
    $this->params[$key] = $value;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function unsetParameter(string $key) {
    if (isset($this->params[$key])) {
      unset($this->params[$key]);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function mergeParameters(array $params) {
    $this->params = array_merge($this->params, $params);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getParameters(): array {
    return $this->params;
  }

  /**
   * {@inheritdoc}
   */
  public function getCdnHostname(): ?string {
    return $this->configFactory
      ->get('bunny_optimizer.settings')
      ->get('cdn_hostname');
  }

  /**
   * {@inheritdoc}
   */
  public static function isAvailable(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSupportedExtensions(): array {
    // There's no distinction between input and output formats in Drupal.
    return array_unique(array_merge(
      static::getSupportedInputExtensions(),
      static::getSupportedOutputExtensions(),
    ));
  }

  /**
   * {@inheritdoc}
   */
  public static function getSupportedInputExtensions(): array {
    return ['jpg', 'jpeg', 'webp', 'gif', 'png', 'tga', 'bmp', 'pbm', 'tiff', 'heic', 'heif'];
  }

  /**
   * {@inheritdoc}
   */
  public static function getSupportedOutputExtensions(): array {
    return ['jpg', 'jpeg', 'webp', 'png', 'gif'];
  }

  /**
   * Load the image info into a static cache.
   */
  protected function loadInfo(): void {
    [$this->width, $this->height, $this->mimeType] = $this->getImageInfo($this->getSource());
  }

  /**
   * Get the dimensions and mime type of a certain image.
   */
  protected function getImageInfo(string $source): ?array {
    $metadata = \Drupal::getContainer()
      ->get(FileMetadataManagerInterface::class)
      ->uri($source);
    $size = $metadata->getMetadata('getimagesize');

    if (!isset($size[0], $size[1], $size['mime'])) {
      return NULL;
    }

    return [$size[0], $size[1], $size['mime']];
  }

}
