<?php

/**
 * @file
 * Hooks and alters for the Bunny Optimizer module.
 */

use Drupal\bunny_optimizer\Entity\BunnyOptimizerImageStyle;
use Drupal\bunny_optimizer\Plugin\ImageEffect\ScaleAndCropImageEffect;

/**
 * Implements @see hook_entity_type_alter().
 */
function bunny_optimizer_entity_type_alter(array &$entity_types): void {
  if (!\Drupal::moduleHandler()->moduleExists('image')) {
    return;
  }

  $toolkitId = \Drupal::getContainer()
    ->get('image.toolkit.manager')
    ->getDefaultToolkitId();

  if ($toolkitId === 'bunny_optimizer') {
    $entity_types['image_style']->setClass(BunnyOptimizerImageStyle::class);
  }
}

/**
 * Implements @see hook_image_effect_info_alter().
 */
function bunny_optimizer_image_effect_info_alter(array &$effects): void {
  if (!\Drupal::moduleHandler()->moduleExists('image')) {
    return;
  }

  $toolkitId = \Drupal::getContainer()
    ->get('image.toolkit.manager')
    ->getDefaultToolkitId();

  if ($toolkitId === 'bunny_optimizer') {
    $effects['image_scale_and_crop']['class'] = ScaleAndCropImageEffect::class;
  }
}
